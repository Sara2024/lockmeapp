package simplilearn.lockme.application;

import java.util.Scanner;

import simplilearn.lockme.model.UserCredentials;
import simplilearn.lockme.model.Users;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Authentication {
	
	 //input data2
    private static Scanner keyboard;
    private static Scanner input;
    //output data
    private static PrintWriter output;
    private static PrintWriter lockerOutput;
    
    //model to store data
    
    private static Users users;
    private static UserCredentials userCredentials;
    
    public static void main(String[] args) {
                   initApp();
                   welcomeScreen();
                   signInOptions();
                   }
    public static void signInOptions() {
                   System.out.println("1. Registration");
                   System.out.println("2. Login");
                   int option = keyboard.nextInt();
                   switch(option) {
                   case 1 :
                   registerUser();
                   break;
                   case 2 :
                   loginUser();
                   break;
                   default :
                                 System.out.println("Please select 1 or 2");
                                 break;
                   }
                   keyboard.close();
                   input.close();
    }
    public static void registerUser() {
                  System.out.println("=====================================");
                   System.out.println("**                                  **");
                   System.out.println("** WELCOME TO REGISTRATION PAGE          **");
                   System.out.println("** Your personal Digital Locker    **");
                  System.out.println("=====================================");
                   System.out.println("Enter Username :");
                   String username = keyboard.next();
                   users.setUsername(username);
                   System.out.println("Enter Password :");
                   String password = keyboard.next();
                   users.setPassword(password);
                   
                   output.println(users.getUsername());
                   output.println(users.getPassword());
                   System.out.println("User Registration Successful");
                   output.close();
    }

    public static void loginUser() {
                  System.out.println("=====================================");
                   System.out.println("**                                  **");
                   System.out.println("** WELCOME TO LOGIN PAGE          **");
                   System.out.println("** Your personal Digital Locker    **");
                  System.out.println("=====================================");
                   System.out.println("Enter Username :");
                   String inpUsername = keyboard.next();
                   boolean found = false;
                   while(input.hasNext() && !found) {
                                 if(input.next().equals(inpUsername)) {
                                                System.out.println("Enter Password :");
                                                String inpPassword = keyboard.next();
                                                if(input.next().equals(inpPassword)) {
                                                              System.out.println("Login Sucessful! 200OK");
                                                              found = true;
                                                              storeCredentials(inpUsername);
                                                              
                                                              break;
                                                }
                                 }
                   }
if(!found) {
    System.out.println("User Not found : Login Failure! 404");
}
}
    public static void welcomeScreen() {
                  System.out.println("=====================================");
                   System.out.println("**                                  **");
                   System.out.println("** Welcome to LockMe.com :          **");
                   System.out.println("** Your personal Digital Locker    **");
                  System.out.println("=====================================");
    }
    
    //store credentials
public static void storeCredentials(String loggedInUser) {
  System.out.println("============================================");
             System.out.println("*                                                                                                                                         ");
             System.out.println("*        WELCOME TO DIGITAL LOCKER STORE YOUR CREDS HERE                             ");
             System.out.println("*                                                                                                                                         ");
  System.out.println("============================================");
             userCredentials.setLoggedInUser(loggedInUser);
             
             System.out.println("Enter Site Name:" );
             String siteName=keyboard.next();
             userCredentials.setSitename(siteName);
             
             System.out.println("Enter User Name:" );
             String username=keyboard.next();
             userCredentials.setUsername(username);
             
             System.out.println("Enter Password:" );
             String password=keyboard.next();
             userCredentials.setPassword(password);
             
             lockerOutput.println(userCredentials.getLoggedInUser());
             lockerOutput.println(userCredentials.getSitename());
             lockerOutput.println(userCredentials.getUsername());
             lockerOutput.println(userCredentials.getPassword());
             
             System.out.println("YOUR CREDS ARE STORED AND SECURED!");

             lockerOutput.close();
    }
public static void initApp() {
    
    File dbFile= new File("Database.txt");
    File lockerFile = new File("Locker-file.txt");
    
    try {
                   //read data from file
                   input = new Scanner (dbFile);
                   
                   //read data from keyboard
                   keyboard =new Scanner (System.in);
                   
                   //output
                   output = new PrintWriter ( new FileWriter (dbFile, true));
                   lockerOutput = new PrintWriter ( new FileWriter (lockerFile, true));
                   
                   users =new Users();
                   userCredentials = new UserCredentials();
                   
    } catch (IOException e) {
                   System.out.println("404: file not found");
    }
    
}
}